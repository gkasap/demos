#!/usr/bin/env bash


spark3-submit --deploy-mode client \
--conf "spark.driver.extraJavaOptions='-Dlog4j.configurationFile=log4j2.properties'" \
--conf "spark.executor.extraJavaOptions='-Dlog4j.configurationFile=log4j2.properties'" \
--files ./log4j2.properties iceberg_demo.py --db default --table ice_table --branch gk_branch --tag tag_2024