# Demo Spark - Iceberg
This is a demo job to illustrate core Iceberg features on CDP 

## Description

## Environment
The following process, has been tested on the following environment:
* CDP 7.1.9 CHF6 (or else 7.1.9.9)
* CDS 3.3 CHF3 for 7.1.9 (or else CDS 3.3.7190.4)

## How to run
In order to run it, you can simply run the [demo.sh](./demo.sh) script that is a wrapper to run the following command:
```shell
spark3-submit --deploy-mode client \
--conf "spark.driver.extraJavaOptions='-Dlog4j.configurationFile=log4j2.properties'" \
--conf "spark.executor.extraJavaOptions='-Dlog4j.configurationFile=log4j2.properties'" \
--files ./log4j2.properties iceberg_demo.py --db default --table ice_table --branch gk_branch --tag tag_2024
```
It is submitting the python script to spark and defines the `database name`, the `table name` , the `branch name` and the `tag name` to use.
In addition, it provides the [log4j2.properties](./log4j2.properties) file to suppress all log messages lower that `error`, as we want to have a clean output.

## Process
Below, we enumerate all the steps, which is similar to what you should see in your terminal, the command to run each time and the respective outputs.
* Step 1 - Create table `default.ice_demo` and insert some data
```shell
Creating table : [default.ice_demo]
+---+----------+---------+------+
| id|first_name|last_name|salary|
+---+----------+---------+------+
|  1|       Max|    Dowel|  4500|
|  2|      John|   Connor|  5000|
|  3|      Jack|    Brown|  8900|
+---+----------+---------+------+
```
* Step 2 - Current status of the table `default.ice_demo`
```shell
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+
| id|first_name|last_name|salary|
+---+----------+---------+------+
|  1|       Max|    Dowel|  4500|
|  2|      John|   Connor|  5000|
|  3|      Jack|    Brown|  8900|
+---+----------+---------+------+
```
* Step 3 - Add a new column
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo ADD COLUMNS (category STRING COMMENT 'Salary category')")
--------------------- 
```
* Step 4 - Current status of the table `default.ice_demo`
```shell
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|    null|
|  2|      John|   Connor|  5000|    null|
|  3|      Jack|    Brown|  8900|    null|
+---+----------+---------+------+--------+
```
* Step 5 - Add a new bucket partition
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo ADD PARTITION FIELD bucket(10,id)")
         
--------------------- 
```
* Step 6 - Insert data
```shell
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       A|
|  6|      Kate| Upertown| 10500|       F|
+---+----------+---------+------+--------+
```
* Step 7 - Current status of the table `default.ice_demo`
```shell
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|    null|
|  2|      John|   Connor|  5000|    null|
|  3|      Jack|    Brown|  8900|    null|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       A|
|  6|      Kate| Upertown| 10500|       F|
+---+----------+---------+------+--------+
```
* Step 8 - Drop the bucket partition
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo DROP PARTITION FIELD bucket(10,id)")
         
--------------------- 
```
* Step 9 - Add partition `category`
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo ADD PARTITION FIELD category")
         
--------------------- 
```
* Step 10 - Current partition status of the table `default.ice_demo`
```shell
--- SQL STATEMENT ---
spark.sql("select * from default.ice_demo.partitions")
         
--------------------- 
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
|   partition|spec_id|record_count|file_count|position_delete_record_count|position_delete_file_count|equality_delete_record_count|equality_delete_file_count|
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
|   {3, null}|      1|           1|         1|                           0|                         0|                           0|                         0|
|   {9, null}|      1|           1|         1|                           0|                         0|                           0|                         0|
|   {0, null}|      1|           1|         1|                           0|                         0|                           0|                         0|
|{null, null}|      0|           3|         2|                           0|                         0|                           0|                         0|
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
````
* Step 11 - Update `category`, based on salary (one category every 5000)
```shell
--- SQL STATEMENT ---
spark.sql("UPDATE default.ice_demo SET category = 'A' WHERE salary < 5000")
         
--------------------- 
--- SQL STATEMENT ---
spark.sql("UPDATE default.ice_demo SET category = 'B' WHERE salary BETWEEN 5000 and 9999")
         
--------------------- 
--- SQL STATEMENT ---
spark.sql("UPDATE default.ice_demo SET category = 'C' WHERE salary BETWEEN 10000 and 14999")
         
--------------------- 
--- SQL STATEMENT ---
spark.sql("UPDATE default.ice_demo SET category = 'D' WHERE salary BETWEEN 15000 and 19999")
         
--------------------- 
--- SQL STATEMENT ---
spark.sql("UPDATE default.ice_demo SET category = 'E' WHERE salary >= 20000")
         
--------------------- 
```
* Step 12 - Insert data
```shell
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  7|     Jesse|    James| 20500|       E|
+---+----------+---------+------+--------+
```
* Step 13 - Current status of the table `default.ice_demo`
```shell
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
+---+----------+---------+------+--------+
```
* Step 14 - Check the history of the table `default.ice_demo`
```shell
--- SQL STATEMENT ---
spark.sql("select * from default.ice_demo.history")
         
--------------------- 
+--------------------+-------------------+-------------------+-------------------+
|     made_current_at|        snapshot_id|          parent_id|is_current_ancestor|
+--------------------+-------------------+-------------------+-------------------+
|2024-06-19 20:49:...|5682474646557897229|               null|               true|
|2024-06-19 20:49:...|6389730180828692949|5682474646557897229|               true|
|2024-06-19 20:49:...|6908579514532290917|6389730180828692949|               true|
|2024-06-19 20:49:...|6215028866942492936|6908579514532290917|               true|
|2024-06-19 20:49:...|2680640163612431582|6215028866942492936|               true|
|2024-06-19 20:49:...|4918264709458744039|2680640163612431582|               true|
|2024-06-19 20:49:...|6874661920584867377|4918264709458744039|               true|
|2024-06-19 20:49:...|9021892633477369894|6874661920584867377|               true|
+--------------------+-------------------+-------------------+-------------------+
```
 - Current view so far
```mermaid
gitGraph
  commit id: "5682474646557897229" tag: "Start"
  commit id: "6389730180828692949"
  commit id: "6908579514532290917"
  commit id: "6215028866942492936"
  commit id: "2680640163612431582"
  commit id: "4918264709458744039"
  commit id: "6874661920584867377"
```
* Step 15 - Check the evolution of the `default.ice_demo` between snapshots '5682474646557897229' and '6874661920584867377'
```shell
* spark.read.format("iceberg").option("start-snapshot-id", 5682474646557897229).option("end-snapshot-id", 6874661920584867377).table(default.ice_demo).show()
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  5|    Austin|   Powers|  7500|       A|
|  6|      Kate| Upertown| 10500|       F|
|  4|     James|     Dean|  5500|       B|
+---+----------+---------+------+--------+
```
* Step 16 - See the data of the table on snapshot `6215028866942492936`
```shell
--- SQL STATEMENT ---
spark.sql("SELECT * from default.ice_demo FOR SYSTEM_VERSION AS OF 6215028866942492936")
         
--------------------- 
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  6|      Kate| Upertown| 10500|       F|
|  1|       Max|    Dowel|  4500|       A|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
+---+----------+---------+------+--------+
```
## BRANCHING AND TAGGING
* Step 17 - Create BRANCH `w_branch`
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo CREATE BRANCH w_branch")
         
--------------------- 
```
* Step 18 - Create TAG `June2024`
```shell
--- SQL STATEMENT ---
spark.sql("ALTER TABLE default.ice_demo CREATE TAG June2024 RETAIN 365 DAYS")
         
--------------------- 
```
* Step 19 - Insert data to BRANCH `w_branch`
```shell
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  8|     Jesse|    James| 29500|       B|
+---+----------+---------+------+--------+
```
* Step 20 - Delete record from branch `w_branch` where `id` is `8`
```shell
--- SQL STATEMENT ---
spark.sql("DELETE FROM default.ice_demo.branch_w_branch WHERE id = '8'")
         
--------------------- 
```
* Step 21 - Insert more data to BRANCH `w_branch`
```shell
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  8|      Kyle|    Maxim| 17500|       D|
|  9|      Mike|   Gordon| 12500|       C|
+---+----------+---------+------+--------+
```
* Step 22 - See the current data of:
```shell
* The `main` branch of table default.ice_demo that all other client see
* The `w_branch` branch
* The `June2024` tag
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
+---+----------+---------+------+--------+

--  Query branch 'w_branch' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
|  8|      Kyle|    Maxim| 17500|       D|
|  9|      Mike|   Gordon| 12500|       C|
+---+----------+---------+------+--------+

--  Query tag 'June2024' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
+---+----------+---------+------+--------+
```
* Step 23 - Check our current references.
```shell
--- SQL STATEMENT ---
spark.sql("select * from default.ice_demo.refs")
         
--------------------- 
+--------+------+-------------------+-----------------------+---------------------+----------------------+
|    name|  type|        snapshot_id|max_reference_age_in_ms|min_snapshots_to_keep|max_snapshot_age_in_ms|
+--------+------+-------------------+-----------------------+---------------------+----------------------+
|w_branch|BRANCH|8738834746981256293|                   null|                 null|                  null|
|June2024|   TAG|9021892633477369894|            31536000000|                 null|                  null|
|    main|BRANCH|9021892633477369894|                   null|                 null|                  null|
+--------+------+-------------------+-----------------------+---------------------+----------------------+
```
- Current status
```mermaid
gitGraph
  commit id: "5682474646557897229" tag: "Start"
  commit id: "6389730180828692949"
  commit id: "6908579514532290917"
  commit id: "6215028866942492936"
  commit id: "2680640163612431582"
  commit id: "4918264709458744039"
  commit id: "6874661920584867377"
  commit id: "9021892633477369894" tag: "June2024 (Retain 365 days)"
  branch  w_branch
  commit id: "8372664355424425536"
  commit id: "7736245536772563736"
  commit id: "8738834746981256293" tag: "w_branch"
```

* Step 24 - Fast forward `main` branch to `w_branch` (snapshot_id: 8738834746981256293).
```shell
--- SQL STATEMENT ---
spark.sql("CALL spark_catalog.system.set_current_snapshot('default.ice_demo', 8738834746981256293)")
         
--------------------- 
```
* Step 25 - Insert data to 'main' branch
```shell
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
| 10|     Pedro|  Sanchez| 23500|       E|
| 11|     Franz|    Kafka| 11500|       C|
| 12|      Scot|   Morris|  8500|       B|
| 13|   Phillip|    Grand| 14500|       C|
+---+----------+---------+------+--------+
```
* Step 26 - See the current data of:
```shell
* The `main` branch of table default.ice_demo that all other client see
* The `w_branch` branch
* The `June2024` tag
--  Query main branch of the table 'default.ice_demo' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
|  8|      Kyle|    Maxim| 17500|       D|
|  9|      Mike|   Gordon| 12500|       C|
| 10|     Pedro|  Sanchez| 23500|       E|
| 11|     Franz|    Kafka| 11500|       C|
| 12|      Scot|   Morris|  8500|       B|
| 13|   Phillip|    Grand| 14500|       C|
+---+----------+---------+------+--------+

--  Query branch 'w_branch' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
|  8|      Kyle|    Maxim| 17500|       D|
|  9|      Mike|   Gordon| 12500|       C|
+---+----------+---------+------+--------+

--  Query tag 'June2024' --
+---+----------+---------+------+--------+
| id|first_name|last_name|salary|category|
+---+----------+---------+------+--------+
|  1|       Max|    Dowel|  4500|       A|
|  2|      John|   Connor|  5000|       B|
|  3|      Jack|    Brown|  8900|       B|
|  4|     James|     Dean|  5500|       B|
|  5|    Austin|   Powers|  7500|       B|
|  6|      Kate| Upertown| 10500|       C|
|  7|     Jesse|    James| 20500|       E|
+---+----------+---------+------+--------+
```
- Current status
```mermaid
gitGraph
  commit id: "5682474646557897229" tag: "Start"
  commit id: "6389730180828692949"
  commit id: "6908579514532290917"
  commit id: "6215028866942492936"
  commit id: "2680640163612431582"
  commit id: "4918264709458744039"
  commit id: "6874661920584867377"
  commit id: "9021892633477369894" tag: "June2024"
  branch  w_branch
  commit id: "8372664355424425536"
  commit id: "7736245536772563736"
  commit id: "8738834746981256293" tag: "w_branch"
  checkout main
  merge w_branch
  commit id: "8826514256347472523" tag: "End"
```

* Step 27 - Current partition status of the table `default.ice_demo`
```shell
--- SQL STATEMENT ---
spark.sql("select * from default.ice_demo.partitions")
         
--------------------- 
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
|   partition|spec_id|record_count|file_count|position_delete_record_count|position_delete_file_count|equality_delete_record_count|equality_delete_file_count|
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
|   {3, null}|      1|           1|         1|                           1|                         1|                           0|                         0|
|   {null, D}|      2|           1|         1|                           0|                         0|                           0|                         0|
|   {null, E}|      2|           2|         2|                           0|                         0|                           0|                         0|
|   {null, B}|      2|           5|         4|                           0|                         0|                           0|                         0|
|   {9, null}|      1|           1|         1|                           1|                         1|                           0|                         0|
|   {null, C}|      2|           4|         3|                           0|                         0|                           0|                         0|
|   {0, null}|      1|           1|         1|                           1|                         1|                           0|                         0|
|{null, null}|      0|           3|         2|                           1|                         2|                           0|                         0|
|   {null, A}|      2|           1|         1|                           0|                         0|                           0|                         0|
+------------+-------+------------+----------+----------------------------+--------------------------+----------------------------+--------------------------+
```