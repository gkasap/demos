import argparse

from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName("GeKas_Demo_iceberg").getOrCreate()

sc = SparkContext

schema_old = ["id", "first_name", "last_name", "salary"]

dataset_wrong = [
    (1, "Max", "Dowel", 4500),
    (2, "John", "Connor", 5000),
    (3, "Jack", "Brown", 8900),
]

schema_new = ["id", "first_name", "last_name", "salary", "category"]

dataset_1 = [
    (4, "James", "Dean", 5500, "B"),
    (5, "Austin", "Powers", 7500, "A"),
    (6, "Kate", "Upertown", 10500, "F"),
]

dataset_2 = [
    (7, "Jesse", "James", 20500, "E"),
]

false_record = [
    (8, "Jesse", "James", 29500, "B"),
]

dataset_3 = [
    (8, "Kyle", "Maxim", 17500, "D"),
    (9, "Mike", "Gordon", 12500, "C"),
]

dataset_4 = [
    (10, "Pedro", "Sanchez", 23500, "E"),
    (11, "Franz", "Kafka", 11500, "C"),
    (12, "Scot", "Morris", 8500, "B"),
    (13, "Phillip", "Grand", 14500, "C"),
]


def __create_table(table_name):
    return f"""CREATE TABLE IF NOT EXISTS {table_name} (
id INT COMMENT 'Employer ID',
first_name STRING COMMENT 'First Name',
last_name STRING COMMENT 'Last Name',
salary BIGINT COMMENT 'Monthly Salary')
USING ICEBERG
TBLPROPERTIES (
'format' = 'iceberg/parquet',
'format-version' = '2')
""".format(table_name=table_name)


def __create_branch__(table_name, branch):
    return f"ALTER TABLE {table_name} CREATE BRANCH {branch}".format(table_name=table_name, branch=branch)


def __create_tag__(table_name, tag):
    return f"ALTER TABLE {table_name} CREATE TAG {tag} RETAIN 365 DAYS".format(table_name=table_name, tag=tag)


def create_table_function(table_name):
    print("Creating table : [{}]".format(table_name))
    create_statement = __create_table(table_name)
    print("".format(create_statement))
    spark.sql("{}".format(create_statement))


def get_snapshot_id_for_ref(table_name, ref_name):
    refs = spark.sql("SELECT * FROM {}.refs".format(table_name)).where(col("name") == ref_name).collect()
    snapshot_id = refs[0].snapshot_id
    return snapshot_id


def insert_data(table, data, schema, branch=None):
    sdf = spark.createDataFrame(data=data, schema=schema)
    sdf.show()
    if branch is None:
        sdf.write.format("iceberg").mode("append").save(table)
    else:
        sdf.write.format("iceberg").option("branch", branch).mode("append").save(table)


def query_table(table, sort="id", branch=None, tag=None):
    if branch is not None:
        print(" --  Query branch '{}' --".format(branch))
        qdf = spark.read.format("iceberg").option("branch", branch).table(table)
    elif tag is not None:
        print(" --  Query tag '{}' --".format(tag))
        qdf = spark.read.format("iceberg").option("tag", tag).table(table)
    else:
        print(" --  Query main branch of the table '{}' --".format(table))
        qdf = spark.read.format("iceberg").table(table)
    qdf.sort(sort).show()


def run_sql(spark_sql_statement=None):
    if spark_sql_statement is None:
        print("No query was provided")
    else:
        print(""" --- SQL STATEMENT ---
        spark.sql("{spark_sql_statement}")
         """.format(spark_sql_statement=spark_sql_statement))
        print(""" --------------------- """)
        tdf = spark.sql(spark_sql_statement)
        return tdf


def run_demo(args):
    table = "{}.{}".format(args.db, args.table)
    branch = args.branch
    tag = args.tag
    step = 0
    step += 1
    print("Step {step} - Create table `{table}`".format(step=step, table=table))
    create_table_function(table)
    insert_data(table=table, data=dataset_wrong, schema=schema_old)
    step += 1
    print("Step {step} - Current status of the table `{table}`".format(step=step, table=table))
    query_table(table=table)
    step += 1
    print("Step {step} - Add a new column".format(step=step))
    run_sql("ALTER TABLE {} ADD COLUMNS (category STRING COMMENT 'Salary category')".format(table))
    step += 1
    print("Step {step} - Current status of the table `{table}`".format(step=step, table=table))
    query_table(table=table)
    step += 1
    print("Step {step} - Add a new bucket partition".format(step=step))
    run_sql("ALTER TABLE {} ADD PARTITION FIELD bucket(10,id)".format(table))
    step += 1
    print("Step {step} - Insert data".format(step=step))
    insert_data(table=table, data=dataset_1, schema=schema_new)
    step += 1
    print("Step {step} - Current status of the table `{table}`".format(step=step, table=table))
    query_table(table=table)
    step += 1
    print("Step {step} - Drop the bucket partition".format(step=step))
    run_sql("ALTER TABLE {} DROP PARTITION FIELD bucket(10,id)".format(table))
    step += 1
    print("Step {step} - Add partition `category`".format(step=step))
    run_sql("ALTER TABLE {} ADD PARTITION FIELD category".format(table))
    step += 1
    print("Step {step} - Current partition status of the table `{table}`".format(step=step, table=table))
    dataframe = run_sql("select * from {}.partitions".format(table))
    dataframe.show()
    step += 1
    print("Step {step} - Update `category`, based on salary (one category every 5000)".format(step=step))
    run_sql("UPDATE {} SET category = 'A' WHERE salary < 5000".format(table))
    run_sql("UPDATE {} SET category = 'B' WHERE salary BETWEEN 5000 and 9999".format(table))
    run_sql("UPDATE {} SET category = 'C' WHERE salary BETWEEN 10000 and 14999".format(table))
    run_sql("UPDATE {} SET category = 'D' WHERE salary BETWEEN 15000 and 19999".format(table))
    run_sql("UPDATE {} SET category = 'E' WHERE salary >= 20000".format(table))
    step += 1
    print("Step {step} - Insert data".format(step=step))
    insert_data(table=table, data=dataset_2, schema=schema_new)
    step += 1
    print("Step {step} - Current status of the table `{table}`".format(step=step, table=table))
    query_table(table=table)
    step += 1
    print("Step {step} - Check the history of the table `{table}`".format(step=step, table=table))
    snapshots_df = run_sql("select * from {}.history".format(table))
    snapshots_df.show()
    snapshots = snapshots_df.collect()
    start_snap = snapshots[0].snapshot_id
    end_snap = snapshots[6].snapshot_id
    step += 1
    print("Step {step} - Check the evolution of the `{table}` between snapshots '{start_snap}' and '{end_snap}'".format(
        step=step, table=table, start_snap=start_snap, end_snap=end_snap))
    print(
        """spark.read.format("iceberg").option("start-snapshot-id", {start_snap}).option("end-snapshot-id", {end_snap}).table({table}).show()""".format(
            table=table, start_snap=start_snap, end_snap=end_snap))
    spark.read.format("iceberg").option("start-snapshot-id", start_snap).option("end-snapshot-id", end_snap).table(
        table).show()
    rand_snap = snapshots[3].snapshot_id
    step += 1
    print("Step {step} - See the data of the table on snapshot `{rand_snap}`".format(step=step, rand_snap=rand_snap))
    dataframe = run_sql("SELECT * from {table} FOR SYSTEM_VERSION AS OF {snapshot}".format(table=table, snapshot=rand_snap))
    dataframe.show()
    print(" -- BRANCHING AND TAGGING -- ")
    step += 1
    print("Step {step} - Create BRANCH `{branch}`".format(step=step, branch=branch))
    create_branch_statement = __create_branch__(table_name=table, branch=branch)
    run_sql(create_branch_statement)
    step += 1
    print("Step {step} - Create TAG `{tag}`".format(step=step, tag=tag))
    create_tag_statement = __create_tag__(table_name=table, tag=tag)
    run_sql(create_tag_statement)
    step += 1
    print("Step {step} - Insert data to BRANCH `{branch}`".format(step=step, branch=branch))
    insert_data(table=table, data=false_record, schema=schema_new, branch=branch)
    step += 1
    print("Step {step} - Delete record from branch `{branch}` where `id` is `8`".format(step=step, branch=branch))
    run_sql("DELETE FROM {table}.branch_{branch} WHERE id = '8'".format(table=table, branch=branch))
    step += 1
    print("Step {step} - Insert more data to BRANCH `{branch}`".format(step=step, branch=branch))
    insert_data(table=table, data=dataset_3, schema=schema_new, branch=branch)
    step += 1
    print("""Step {step} - See the current data of:
    * The `main` branch of table {table} that all other client see
    * The `{branch}` branch
    * The `{tag}` tag""".format(step=step, table=table, branch=branch, tag=tag))
    query_table(table=table)
    query_table(table=table, branch=branch)
    query_table(table=table, tag=tag)
    # FAST FORWARD
    step += 1
    print("Step {step} - Check our current references.".format(step=step))
    snapdf = run_sql("select * from {}.refs".format(table))
    snapdf.show()
    branch_id = get_snapshot_id_for_ref(table_name=table, ref_name=branch)
    step += 1
    print("Step {step} - Fast forward `main` branch to `{branch}` (snapshot_id: {snapshot}).".format(step=step,
                                                                                                     branch=branch,
                                                                                                     snapshot=branch_id))
    run_sql("CALL spark_catalog.system.set_current_snapshot('{table}', {snapshot})".format(table=table,
                                                                                             snapshot=branch_id))
    step += 1
    print("Step {step} - Insert data to 'main' branch".format(step=step))
    insert_data(table=table, data=dataset_4, schema=schema_new)
    # FINAL STATUS
    step += 1
    print("""Step {step} - See the current data of:
    * The `main` branch of table {table} that all other client see
    * The `{branch}` branch
    * The `{tag}` tag""".format(step=step, table=table, branch=branch, tag=tag))
    query_table(table=table)
    query_table(table=table, branch=branch)
    query_table(table=table, tag=tag)
    step += 1
    print("Step {step} - Current partition status of the table `{table}`".format(step=step, table=table))
    dataframe = run_sql("select * from {}.partitions".format(table))
    dataframe.show()


def main():
    print("Demo spark and Iceberg!")
    parser = argparse.ArgumentParser(description="Demo Spark and Iceberg!")
    parser.add_argument('--db', action='store', dest='db', default='default', help="Database Name")
    parser.add_argument('--table', action='store', dest='table', default='ice_table', help="Table Name")
    parser.add_argument('--branch', action='store', dest='branch', default='my_brach', help="Branch Name")
    parser.add_argument('--tag', action='store', dest='tag', default='tag1', help="Tag Name")

    parser.set_defaults(function=run_demo)
    args = parser.parse_args()
    run_demo(args=args)


if __name__ == "__main__":
    main()
